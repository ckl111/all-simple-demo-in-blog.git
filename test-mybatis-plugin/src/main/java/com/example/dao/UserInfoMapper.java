package com.example.dao;

import com.example.model.UserInfo;
import com.example.model.UserInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserInfoMapper {
    /**
     *
     * @mbg.generated
     */
    long countByExample(UserInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int deleteByExample(UserInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int insert(UserInfo record);

    /**
     *
     * @mbg.generated
     */
    int insertSelective(UserInfo record);

    /**
     *
     * @mbg.generated
     */
    List<UserInfo> selectByExample(UserInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") UserInfo record, @Param("example") UserInfoExample example);

    /**
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") UserInfo record, @Param("example") UserInfoExample example);
}